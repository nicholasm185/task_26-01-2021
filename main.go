package main

import (
	"fmt"
	"log"
	"os"

	"github.com/joho/godotenv"
	dl "gitlab.com/nicholasm185/task_26-01-2021/delivery/gRPC"
	rp "gitlab.com/nicholasm185/task_26-01-2021/repository/mysql"
	uc "gitlab.com/nicholasm185/task_26-01-2021/usecase"
)

func main(){
	err := godotenv.Load(".env")
	if err != nil{
		log.Fatal("error getting .env file")
	}
	fmt.Println("Initiating application")

	mysqlUser := rp.NewUserRepo(os.Getenv("DB_URL"), 
		os.Getenv("DB_NAME"), 
		os.Getenv("DB_USER"), 
		os.Getenv("DB_PASS"))

	defer mysqlUser.Close()

	userUsecase := uc.NewUserUsecase(mysqlUser)
	userDelivery := dl.NewUserGRPCDelivery(userUsecase)
	err1 := userDelivery.Serve()
	if err1 != nil{
		panic(err1.Error())
	}
	fmt.Println("Application running")
}