package test

import "gitlab.com/nicholasm185/task_26-01-2021/model"

var User1 = model.User{
	UserID:     1,
	Name:       "A",
	PNumber:    123456789,
	PassHash:   "abcd",
	DateJoined: "2021-02-01 14:46:13"}

var User2 = model.User{
	UserID:     2,
	Name:       "B",
	PNumber:    987654312,
	PassHash:   "dcba",
	DateJoined: "2021-04-01 14:46:13"}

var User1Modifier = model.UserProto{
	Name:       "C",
	PNumber:    123456789,
	PassHash:   "abcd"}

var User1ModifierFail = model.UserProto{
	Name:       "A",
	PNumber:    123456789,
	PassHash:   "abcd"}