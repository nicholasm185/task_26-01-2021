package model

import (
)

// User is the model of a record in db
type User struct {
	UserID		int		`json:"userID"`
	Name		string	`json:"name"`
	PNumber		int64	`json:"pNumber"`
	PassHash	string	`json:"passHash"`
	DateJoined	string	`json:"dateJoined"`
}

// UserProto is unrecorded data
type UserProto struct {
	Name		string	`json:"name"`
	PNumber		int64	`json:"pNumber"`
	PassHash	string	`json:"passHash"`
	DateJoined	string	`json:"dateJoined"`
}

// UserUsecase is the interface for functions to be exposed by a user usecase
type UserUsecase interface {
	GetAll() ([]User,error)
	InsertOne(userProto UserProto) error
	Update(userID int, userProto UserProto) error
	GetOne(userID int) (User,error)
}

// UserRepository is the interface for functions be exposed by a user repository
type UserRepository interface {
	GetAll() ([]User,error)
	InsertOne(userProto UserProto) error
	Update(userID int, userProto UserProto) error
	GetOne(userID int) (User,error)
	Close() error
}

// UserDelivery is the interface for functions be exposed by a user delivery
type UserDelivery interface {
	Serve() error
}