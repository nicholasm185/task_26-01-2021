package repository

import (
	"database/sql"
	"log"
	"regexp"
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/nicholasm185/task_26-01-2021/model"
)

var testModel1 = &model.User{
	UserID:     2,
	Name:       "a",
	PNumber:    123456789,
	PassHash:   "abcdefghi",
	DateJoined: "2021-04-01 14:46:13",
}
var testModel2 = &model.User{
	UserID:     6,
	Name:       "b",
	PNumber:    98765431,
	PassHash:   "zxcvvbn,.",
	DateJoined: "2021-07-05 14:46:13",
}
var testModel3 = &model.UserProto{
	Name:       "c",
	PNumber:    546132146,
	PassHash:   "alskdjfh,.",
	DateJoined: "2021-09-05 12:22:13",
}

// NewTestingUserRepo receives a db mock to return mysqlUserRepository instance for testing
func NewTestingUserRepo(db *sql.DB) model.UserRepository {
	mydb := mysqlUserRepository{db: db}
	return &mydb
}

func NewDBMock() (*sql.DB, sqlmock.Sqlmock) {
	db, mock, err := sqlmock.New()
	if err != nil {
		log.Fatalf("error opening db stub: %s", err)
	}
	return db, mock
}

func TestGetOne(t *testing.T) {
	db, mock := NewDBMock()
	repo := NewTestingUserRepo(db)
	defer func() {
		repo.Close()
	}()
	query := "SELECT * FROM users WHERE userID = ?"

	rows := sqlmock.NewRows([]string{"userID", "name", "pNumber", "passHash", "dateJoined"}).
		AddRow(testModel1.UserID, testModel1.Name, testModel1.PNumber, testModel1.PassHash, testModel1.DateJoined)

	mock.ExpectQuery(regexp.QuoteMeta(query)).WithArgs(testModel1.UserID).WillReturnRows(rows)

	res, err := repo.GetOne(testModel1.UserID)
	assert.NotNil(t, res)
	assert.NoError(t, err)
	assert.Equal(t, testModel1.Name, res.Name)
}

func TestGetAll(t *testing.T) {
	db, mock := NewDBMock()
	repo := NewTestingUserRepo(db)
	defer func() {
		repo.Close()
	}()
	query := "SELECT * FROM users"

	rows := sqlmock.NewRows([]string{"userID", "name", "pNumber", "passHash", "dateJoined"}).
		AddRow(testModel1.UserID, testModel1.Name, testModel1.PNumber, testModel1.PassHash, testModel1.DateJoined)
	rows.AddRow(testModel2.UserID, testModel2.Name, testModel2.PNumber, testModel2.PassHash, testModel2.DateJoined)

	mock.ExpectQuery(regexp.QuoteMeta(query)).WillReturnRows(rows)

	res, err := repo.GetAll()
	assert.NotNil(t, res)
	assert.NoError(t, err)
	assert.Equal(t, testModel1.Name, res[0].Name)
	assert.Equal(t, testModel2.Name, res[1].Name)
}

func TestInsertOne(t *testing.T) {
	db, mock := NewDBMock()
	repo := NewTestingUserRepo(db)
	defer func() {
		repo.Close()
	}()
	query := "INSERT INTO users (`userID`, `name`, `pNumber`, `passHash`, `dateJoined`) VALUES (null, ?, ?, ?, ?)"

	mock.ExpectExec(regexp.QuoteMeta(query)).WithArgs(testModel3.Name, testModel3.PNumber, testModel3.PassHash, testModel3.DateJoined).WillReturnResult(sqlmock.NewResult(0, 1))
	err := repo.InsertOne(*testModel3)
	assert.NoError(t, err)
}

func TestUpdate(t *testing.T) {
	db, mock := NewDBMock()
	repo := NewTestingUserRepo(db)
	defer func() {
		repo.Close()
	}()
	query := "UPDATE `users` SET `name` = ?, `pNumber` = ?, `passHash` = ? WHERE `users`.`userID` = ?"

	mock.ExpectExec(regexp.QuoteMeta(query)).WithArgs(testModel3.Name, testModel3.PNumber, testModel3.PassHash, 1).WillReturnResult(sqlmock.NewResult(0, 1))
	err := repo.Update(1, *testModel3)
	assert.NoError(t, err)
}