package repository

import (
	"database/sql"
	"errors"
	"fmt"
	"log"

	_ "github.com/go-sql-driver/mysql"

	"gitlab.com/nicholasm185/task_26-01-2021/model"
)

type mysqlUserRepository struct{
	db *sql.DB
}

// NewUserRepo returns a type mysqlUserRepository which uses interface model.UserRepository
func NewUserRepo(url string, dbName string, dbUser string, dbPass string) model.UserRepository{
	mydb := mysqlUserRepository{}
	mydb.connect(url,dbName,dbUser,dbPass)
	return &mydb
}

// Connect opens connection to mysqldb
func (mydb *mysqlUserRepository) connect(url string, dbName string, dbUser string, dbPass string){
	db, err := sql.Open("mysql", fmt.Sprintf("%s:%s@tcp(%s)/%s", dbUser, dbPass, url, dbName))
	mydb.db = db
	if err != nil {
		log.Fatal(err.Error())
	}

	defer fmt.Println("db seems to have connected")
	// defer db.Close()
}

func (mydb *mysqlUserRepository) Close() error{
	err := mydb.db.Close()
	return err
}

func (mydb *mysqlUserRepository) GetAll() ([]model.User,error){
	var res []model.User
	results, err1 := mydb.db.Query("SELECT * FROM users")
	if err1 != nil {
		// panic(err1.Error())
		return res,err1
	}

	for results.Next(){
		var user model.User
		err2 := results.Scan(&user.UserID, &user.Name, &user.PNumber, &user.PassHash, &user.DateJoined)
		if err2 != nil{
			fmt.Println("db no results found")
			// panic(err2.Error())
			return res, err2
		}
		// fmt.Println(user)
		res = append(res, user)
	}
	return res,nil
}

func (mydb *mysqlUserRepository) GetOne(userID int) (model.User,error){
	var user model.User
	query := "SELECT * FROM users WHERE userID = ?"
	res := mydb.db.QueryRow(query, userID)
	err := res.Scan(&user.UserID, &user.Name, &user.PNumber, &user.PassHash, &user.DateJoined)
	if err != nil {
		fmt.Println("db no results found")
		// panic(err.Error())
		return user,err
	}
	// fmt.Println(user)
	
	return user,nil
}

func (mydb *mysqlUserRepository) InsertOne(userProto model.UserProto) error{
	query := "INSERT INTO users (`userID`, `name`, `pNumber`, `passHash`, `dateJoined`) VALUES (null, ?, ?, ?, ?)"
	_, err := mydb.db.Exec(query, userProto.Name, userProto.PNumber, userProto.PassHash, userProto.DateJoined)

	if err != nil {
		// panic(err.Error())
		return err
	}
	fmt.Println("repo: successfully inserted")
	return nil
}

func (mydb *mysqlUserRepository) Update(userID int, userProto model.UserProto) error {
	if userID < 0{
		return errors.New("Invalid userID")
	}
	if userProto == (model.UserProto{}) {
		return errors.New("Invalid data")
	}
	query := "UPDATE `users` SET `name` = ?, `pNumber` = ?, `passHash` = ? WHERE `users`.`userID` = ?"
	_, err := mydb.db.Exec(query, userProto.Name, userProto.PNumber, userProto.PassHash, userID)
	if err != nil {
		// panic(err.Error())
		return err
	}
	fmt.Println("repo: successfully updated")
	return nil
}