package main

import (
	"log"
	"time"

	"gitlab.com/nicholasm185/task_26-01-2021/delivery/grpc"
	"gitlab.com/nicholasm185/task_26-01-2021/model"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
)

var testData = &model.UserProto{
	Name: "some user",
	PNumber: 654231978,
	PassHash: "quiwoeia;a4a",
	DateJoined: time.Now().String(),
}

func main() {
	conn, err := grpc.Dial("localhost:8080", grpc.WithInsecure())
	if err != nil {
		panic(err.Error())
	}

	client := delivery.NewUserDeliveryClient(conn)
	defer conn.Close()

	// getOneRequest := delivery.GetOneRequest{
	// 	UserID: 6,
	// }

	insertOneRequest := delivery.InsertOneRequest{
		Name: testData.Name,
		PNumber: testData.PNumber,
		PassHash: testData.PassHash,
	}

	// response, err := client.GetOne(context.Background(), &getOneRequest)
	// if err != nil{
	// 	panic(err.Error())
	// }
	// log.Println(response)

	// response, err := client.GetAll(context.Background(), &delivery.GetAllRequest{})
	// if err != nil{
	// 	panic(err.Error())
	// }
	// log.Println(response)

	response1, err1 := client.InsertOne(context.Background(), &insertOneRequest)
	if err != nil{
		panic(err1.Error())
	}
	log.Println(response1)
}