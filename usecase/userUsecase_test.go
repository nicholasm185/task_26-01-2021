package usecase

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/nicholasm185/task_26-01-2021/mock"
	"gitlab.com/nicholasm185/task_26-01-2021/model"
	"gitlab.com/nicholasm185/task_26-01-2021/test"
)	
func TestGetAll(t *testing.T) {
	mockUserRepo := new(mock.MockUserRepository)

	// setup the repo mock
	mockUserRepo.On("GetAll").Return([]model.User{test.User1, test.User2}, nil)

	// Initiate and call the tested function
	testUserUsecase := NewUserUsecase(mockUserRepo)
	res, _ := testUserUsecase.GetAll()

	// Assert Behaviour
	mockUserRepo.AssertExpectations(t)

	// Assert Data
	assert.Equal(t, 1, res[0].UserID)
	assert.Equal(t, "A", res[0].Name)
	assert.Equal(t, int64(123456789), res[0].PNumber)
	assert.Equal(t, "abcd", res[0].PassHash)
	assert.Equal(t, "2021-02-01 14:46:13", res[0].DateJoined)
	assert.Equal(t, 2, res[1].UserID)
	assert.Equal(t, "B", res[1].Name)
	assert.Equal(t, int64(987654312), res[1].PNumber)
	assert.Equal(t, "dcba", res[1].PassHash)
	assert.Equal(t, "2021-04-01 14:46:13", res[1].DateJoined)
}

func TestGetOne(t *testing.T) {
	mockUserRepo := new(mock.MockUserRepository)

	mockUserRepo.On("GetOne", 1).Return(test.User1, nil)

	testUserUsecase := NewUserUsecase(mockUserRepo)
	res, _ := testUserUsecase.GetOne(1)

	mockUserRepo.AssertExpectations(t)

	assert.Equal(t, test.User1.UserID, res.UserID)
}

func TestGetOneFail(t *testing.T) {
	mockUserRepo := new(mock.MockUserRepository)

	testUserUsecase := NewUserUsecase(mockUserRepo)
	_, err := testUserUsecase.GetOne(-1)

	assert.EqualError(t, errors.New("Invalid ID"), err.Error())
}

func Update(t *testing.T) {
	mockUserRepo := new(mock.MockUserRepository)

	mockUserRepo.On("Update", 1, test.User1Modifier).Return(nil)

	testUserUsecase := NewUserUsecase(mockUserRepo)
	err := testUserUsecase.Update(1, test.User1Modifier)

	mockUserRepo.AssertExpectations(t)

	assert.ErrorIs(t, nil, err)
}

func UpdateFailID(t *testing.T){
	mockUserRepo := new(mock.MockUserRepository)
	testUserUsecase := NewUserUsecase(mockUserRepo)
	err := testUserUsecase.Update(-2, test.User1Modifier)

	mockUserRepo.AssertExpectations(t)

	assert.EqualError(t, errors.New("Invalid ID"), err.Error())
}

func UpdateFailIdentical(t *testing.T){
	mockUserRepo := new(mock.MockUserRepository)

	mockUserRepo.On("GetOne", 1).Return(test.User1, nil)

	testUserUsecase := NewUserUsecase(mockUserRepo)
	err := testUserUsecase.Update(1, test.User1ModifierFail)

	mockUserRepo.AssertExpectations(t)

	assert.EqualError(t, errors.New("No changes made"), err.Error())
}