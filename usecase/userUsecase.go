package usecase

import (
	"fmt"
	"errors"
	"crypto/md5"
	"gitlab.com/nicholasm185/task_26-01-2021/model"
	"io"
	"time"
	"log"
)

type userUsecase struct{
	userRepo model.UserRepository
}

// NewUserUsecase returns an instance of userUsecase type with injected rp
func NewUserUsecase(rp model.UserRepository) model.UserUsecase{
	return &userUsecase{
		userRepo: rp,
	}
}

func (userUse *userUsecase) GetAll() ([]model.User,error){
	res,err := userUse.userRepo.GetAll()
	if err != nil {
		log.Fatal("uc: error getting all")
		return res, err
	}
	for _, user := range res{
		fmt.Println(user)
	}
	return res, nil
}

func (userUse *userUsecase) GetOne(userID int) (model.User,error){
	if userID < 0{
		return model.User{}, errors.New("Invalid ID")
	}
	res, err := userUse.userRepo.GetOne(userID)
	if err != nil{
		log.Fatal("uc: error getting one")
		return res,err
	}
	fmt.Println(res)
	return res,err
}

func (userUse *userUsecase) InsertOne(userProto model.UserProto) error{
	h := md5.New()
	io.WriteString(h, userProto.PassHash)
	userProto.PassHash = string(h.Sum(nil))
	userProto.DateJoined = time.Now().Local().String()
	return userUse.userRepo.InsertOne(userProto)
}

func (userUse *userUsecase) Update(userID int, userProto model.UserProto) error{
	if userID < 0{
		return errors.New("Invalid ID")
	}
	cur, err := userUse.userRepo.GetOne(userID)
	if err != nil{
		return err
	}
	h := md5.New()
	io.WriteString(h, userProto.PassHash)
	userProto.PassHash = string(h.Sum(nil))

	if cur.Name == userProto.Name && cur.PNumber == userProto.PNumber && cur.PassHash == userProto.PassHash{
		return errors.New("No changes made")
	}

	return userUse.userRepo.Update(userID, userProto)
}
