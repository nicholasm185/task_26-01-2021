package delivery

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/nicholasm185/task_26-01-2021/mock"
	"gitlab.com/nicholasm185/task_26-01-2021/model"
	"gitlab.com/nicholasm185/task_26-01-2021/test"

	"golang.org/x/net/context"
)

func newUserGRPCDeliveryTest(uc model.UserUsecase) userGRPCDelivery{
	return userGRPCDelivery{
		userUsecase: uc,
	}
}

func TestGetAll(t *testing.T){
	mockUsecase := new(mock.MockUserUsecase)

	mockUsecase.On("GetAll").Return([]model.User{test.User1, test.User2}, nil)

	testDelivery := newUserGRPCDeliveryTest(mockUsecase)
	res, _ := testDelivery.GetAll(context.Background(), new(GetAllRequest))

	mockUsecase.AssertExpectations(t)

	assert.Equal(t, "A", res.GetUsers()[0].Name)
}

func TestInsertOne(t *testing.T){
	mockUsecase := new(mock.MockUserUsecase)

	mockUsecase.On("InsertOne", test.User1Modifier).Return(nil)
	testDelivery := newUserGRPCDeliveryTest(mockUsecase)
	res, err := testDelivery.InsertOne(context.Background(), &InsertOneRequest{
		Name: test.User1Modifier.Name,
		PNumber: test.User1Modifier.PNumber,
		PassHash: test.User1Modifier.PassHash,
	})

	mockUsecase.AssertExpectations(t)

	assert.Equal(t, "No error", res.Error)
	assert.Nil(t, err)
}

func TestUpdate(t *testing.T){
	mockUsecase := new(mock.MockUserUsecase)

	mockUsecase.On("Update", 1, test.User1Modifier).Return(nil)
	testDelivery := newUserGRPCDeliveryTest(mockUsecase)
	res, err := testDelivery.Update(context.Background(), &UpdateRequest{
		UserID: 1,
		Name: test.User1Modifier.Name,
		PNumber: test.User1Modifier.PNumber,
		PassHash: test.User1Modifier.PassHash,
	})

	mockUsecase.AssertExpectations(t)

	assert.Equal(t, "No error", res.Error)
	assert.Nil(t, err)
}

func TestGetOne(t *testing.T){
	mockUsecase := new(mock.MockUserUsecase)
	mockUsecase.On("GetOne", 1).Return(test.User1, nil)
	testDelivery := newUserGRPCDeliveryTest(mockUsecase)
	res, err := testDelivery.GetOne(context.Background(), &GetOneRequest{
		UserID: 1,
	})

	mockUsecase.AssertExpectations(t)

	assert.Equal(t, "A", res.Name)
	assert.Nil(t, err)
}