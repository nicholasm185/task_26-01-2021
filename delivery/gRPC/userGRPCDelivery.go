package delivery

import (
	"fmt"
	"log"
	"net"

	"golang.org/x/net/context"
	"google.golang.org/grpc"

	"gitlab.com/nicholasm185/task_26-01-2021/model"
)

type userGRPCDelivery struct {
	userUsecase model.UserUsecase
	UnimplementedUserDeliveryServer
}

// NewUserGRPCDelivery creates an instance of this delivery service
func NewUserGRPCDelivery(uc model.UserUsecase) model.UserDelivery{
	return &userGRPCDelivery{
		userUsecase: uc,
	}
}

func (s *userGRPCDelivery) GetAll(ctx context.Context, message *GetAllRequest) (*GetAllReply, error){
	res, err := s.userUsecase.GetAll()
	var users []*User
	if err != nil{
		return &GetAllReply{
			Users: users,
			Error: err.Error(),
		}, err
	}
	for _, item := range res{
		user := User{
			UserID: int64(item.UserID),
			Name: item.Name,
			PNumber: item.PNumber,
			PassHash: item.PassHash,
			DateJoined: item.DateJoined,
		}
		users = append(users, &user)
	}
	return &GetAllReply{
		Users: users,
		Error: "No errors",
	}, err
}

func (s *userGRPCDelivery) InsertOne(ctx context.Context, message *InsertOneRequest) (*InsertOneReply, error){
	err := s.userUsecase.InsertOne(model.UserProto{
		Name: message.GetName(),
		PNumber: message.GetPNumber(),
		PassHash: message.GetPassHash(),
	})
	if err != nil {
		return &InsertOneReply{
			Error: err.Error(),
		}, err
	}
	return &InsertOneReply{
		Error: "No error",
	}, nil
}

func (s *userGRPCDelivery) Update(ctx context.Context, message *UpdateRequest) (*UpdateReply, error){
	user := model.UserProto{
		Name: message.GetName(),
		PNumber: message.GetPNumber(),
		PassHash: message.GetPassHash(),
	}
	err := s.userUsecase.Update(int(message.GetUserID()), user)
	if err != nil {
		return &UpdateReply{
			Error: err.Error(),
		}, err
	}
	return &UpdateReply{
		Error: "No error",
	}, nil
}

func (s *userGRPCDelivery) GetOne(ctx context.Context, message *GetOneRequest) (*User, error){
	res, err := s.userUsecase.GetOne(int(message.GetUserID()))
	return &User{
		UserID: int64(res.UserID),
		Name: res.Name,
		PNumber: res.PNumber,
		PassHash: res.PassHash,
		DateJoined: res.DateJoined,
	}, err
}

// Serve the gRPC service
func (s *userGRPCDelivery) Serve() error {
	lis, err := net.Listen("tcp", ":8080")
	if err != nil {
		fmt.Println("delivery: ")
		log.Fatal(err.Error())
		return err
	}

	grpcServer := grpc.NewServer()
	RegisterUserDeliveryServer(grpcServer, s)

	if err := grpcServer.Serve(lis); err != nil {
		fmt.Println("delivery: ")
		log.Fatal(err.Error())
		return err
	}
	return nil
}