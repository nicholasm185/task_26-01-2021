module gitlab.com/nicholasm185/task_26-01-2021

go 1.15

require (
	github.com/DATA-DOG/go-sqlmock v1.5.0
	github.com/go-sql-driver/mysql v1.5.0
	github.com/joho/godotenv v1.3.0
	github.com/stretchr/testify v1.7.0
	golang.org/x/net v0.0.0-20190311183353-d8887717615a
	google.golang.org/grpc v1.35.0
	google.golang.org/protobuf v1.25.0
)
