package mock

import (
	"github.com/stretchr/testify/mock"
	"gitlab.com/nicholasm185/task_26-01-2021/model"
)

type MockUserUsecase struct{
	mock.Mock
}

func (mock *MockUserUsecase) GetAll() ([]model.User, error){
	args := mock.Called()
	result := args.Get(0)
	return result.([]model.User), args.Error(1)
}

func (mock *MockUserUsecase) InsertOne(userProto model.UserProto) error {
	args := mock.Called(userProto)
	return args.Error(0)
}

func (mock *MockUserUsecase) Update(userID int, userProto model.UserProto) error {
	args := mock.Called(userID, userProto)
	return args.Error(0)
}

func (mock *MockUserUsecase) GetOne(userID int) (model.User,error) {
	args := mock.Called(userID)
	result := args.Get(0)
	return result.(model.User), args.Error(1)
}