package mock

import (
	"github.com/stretchr/testify/mock"
	"gitlab.com/nicholasm185/task_26-01-2021/model"
)

type MockUserRepository struct{
	mock.Mock
}

func (mock *MockUserRepository) GetAll() ([]model.User, error){
	args := mock.Called()
	result := args.Get(0)
	return result.([]model.User), args.Error(1)
}

func (mock *MockUserRepository) InsertOne(userProto model.UserProto) error {
	args := mock.Called(userProto)
	return args.Error(0)
}

func (mock *MockUserRepository) Update(userID int, userProto model.UserProto) error {
	args := mock.Called(userID, userProto)
	return args.Error(0)
}

func (mock *MockUserRepository) GetOne(userID int) (model.User,error) {
	args := mock.Called(userID)
	result := args.Get(0)
	return result.(model.User), args.Error(1)
}

func (mock *MockUserRepository) Close() error{
	return nil
}